import SquirrelRNG from '../package/SquirrelNoise5';
import { describe, expect, it } from 'vitest';

describe('#squirrel noise 5', () => {
  it('getSeed returns 420 when seed passed in is 420', () => {
    const rng = new SquirrelRNG(420, 69);
    expect(rng.getSeed()).toBe(420);
  });
  it('getSeed returns 999 when seed is reset to 999', () => {
    const rng = new SquirrelRNG(420, 69);
    expect(rng.getSeed()).toBe(420);
    rng.resetSeed(999, 777);
    expect(rng.getSeed()).toBe(999);
  });
  it('getPosition returns 69 when position passed in is 69', () => {
    const rng = new SquirrelRNG(420, 69);
    expect(rng.getPosition()).toBe(69);
  });
  it('getPosition returns 777 when position is reset to 999', () => {
    const rng = new SquirrelRNG(420, 69);
    expect(rng.getSeed()).toBe(420);
    rng.resetSeed(999, 777);
    expect(rng.getSeed()).toBe(999);
  });
  it('getPosition returns position that was passed in', () => {
    const rng = new SquirrelRNG(420, 69);
    expect(rng.getPosition()).toBe(69);
    rng.setPosition(123123123);
    expect(rng.getPosition()).toBe(123123123);
  });

  it('rollRandomIntLessThan returns in sequence 11, 11, 12, 0 with seed=42 and int less than 14', () => {
    const rng = new SquirrelRNG(42);
    expect(rng.getPosition()).toBe(0);
    expect(rng.rollRandomIntLessThan(14)).toBe(11);
    expect(rng.getPosition()).toBe(1);
    expect(rng.rollRandomIntLessThan(14)).toBe(11);
    expect(rng.getPosition()).toBe(2);
    expect(rng.rollRandomIntLessThan(14)).toBe(12);
    expect(rng.getPosition()).toBe(3);
    expect(rng.rollRandomIntLessThan(14)).toBe(0);
    expect(rng.getPosition()).toBe(4);
  });
  it('rollRandomIntInRange returns in sequence 3, 9, 9, 11 with seed=17 and range 1 to 12', () => {
    const rng = new SquirrelRNG(17);
    expect(rng.getPosition()).toBe(0);
    expect(rng.rollRandomIntInRange(1, 12)).toBe(3);
    expect(rng.getPosition()).toBe(1);
    expect(rng.rollRandomIntInRange(1, 12)).toBe(9);
    expect(rng.getPosition()).toBe(2);
    expect(rng.rollRandomIntInRange(1, 12)).toBe(9);
    expect(rng.getPosition()).toBe(3);
    expect(rng.rollRandomIntInRange(1, 12)).toBe(11);
    expect(rng.getPosition()).toBe(4);
  });
  it('rollRandomIntInRange returns in sequence -36, -96, -71, -52 with seed=17 and range -100 to 1', () => {
    const rng = new SquirrelRNG(40000);
    expect(rng.getPosition()).toBe(0);
    expect(rng.rollRandomIntInRange(-100, 1)).toBe(-36);
    expect(rng.getPosition()).toBe(1);
    expect(rng.rollRandomIntInRange(-100, 1)).toBe(-96);
    expect(rng.getPosition()).toBe(2);
    expect(rng.rollRandomIntInRange(-100, 1)).toBe(-71);
    expect(rng.getPosition()).toBe(3);
    expect(rng.rollRandomIntInRange(-100, 1)).toBe(-52);
    expect(rng.getPosition()).toBe(4);
  });
  it('rollRandomChance returns in sequence t,f,f,f with seed=17 and probabilityOfReturningTrue=.7', () => {
    const rng = new SquirrelRNG(17);
    expect(rng.getPosition()).toBe(0);
    expect(rng.rollRandomChance(0.7)).toBe(true);
    expect(rng.getPosition()).toBe(1);
    expect(rng.rollRandomChance(0.7)).toBe(false);
    expect(rng.getPosition()).toBe(2);
    expect(rng.rollRandomChance(0.7)).toBe(false);
    expect(rng.getPosition()).toBe(3);
    expect(rng.rollRandomChance(0.7)).toBe(false);
    expect(rng.getPosition()).toBe(4);
  });

  it('Get1dNoiseUint returns 2755490221 with x=1 and seed=12341234', () => {
    expect(SquirrelRNG.Get1dNoiseUint(1, 12341234)).toBe(2755490221);
  });
  it('Get2dNoiseUint returns 2995393138 with x=1, y=1 and seed=12341234', () => {
    expect(SquirrelRNG.Get2dNoiseUint(1, 1, 12341234)).toBe(2995393138);
  });
  it('Get3dNoiseUint returns 2487067383 with x=1, y=2, z=3 and seed=12341234', () => {
    expect(SquirrelRNG.Get3dNoiseUint(1, 2, 3, 12341234)).toBe(2487067383);
  });
  it('Get4dNoiseUint returns 3184868871 with x=100, y=100, z=2, t=1 and seed=12341234', () => {
    expect(SquirrelRNG.Get4dNoiseUint(100, 100, 2, 1, 12341234)).toBe(
      3184868871
    );
  });

  it('Get1dNoiseZeroToOne returns 0.6415625618867489 with x=1 and seed=12341234', () => {
    expect(SquirrelRNG.Get1dNoiseZeroToOne(1, 12341234)).toBe(
      0.6415625618867489
    );
  });
  it('Get2dNoiseZeroToOne returns 0.8967186829300408 with x=1, y=2 and seed=12341234', () => {
    expect(SquirrelRNG.Get2dNoiseZeroToOne(1, 2, 12341234)).toBe(
      0.8967186829300408
    );
  });
  it('Get3dNoiseZeroToOne returns 0.5790654997292592 with x=1, y=2, z=3 and seed=12341234', () => {
    expect(SquirrelRNG.Get3dNoiseZeroToOne(1, 2, 3, 12341234)).toBe(
      0.5790654997292592
    );
  });
  it('Get4dNoiseZeroToOne returns 0.7476497403689776 with x=1, y=2, z=3, t=4 and seed=12341234', () => {
    expect(SquirrelRNG.Get4dNoiseZeroToOne(1, 2, 3, 4, 12341234)).toBe(
      0.7476497403689776
    );
  });

  it('Get1dNoiseNegOneToOne returns -0.7168748768590739 with x=1 and seed=12341234', () => {
    expect(SquirrelRNG.Get1dNoiseNegOneToOne(1, 12341234)).toBe(
      -0.7168748768590739
    );
  });
  it('Get2dNoiseNegOneToOne returns -0.2065626346536738 with x=1, y=2 and seed=12341234', () => {
    expect(SquirrelRNG.Get2dNoiseNegOneToOne(1, 2, 12341234)).toBe(
      -0.2065626346536738
    );
  });
  it('Get3dNoiseNegOneToOne returns -0.8418690012031557 with x=1, y=2, z=3 and seed=12341234', () => {
    expect(SquirrelRNG.Get3dNoiseNegOneToOne(1, 2, 3, 12341234)).toBe(
      -0.8418690012031557
    );
  });
  it('Get4dNoiseNegOneToOne returns -0.5047005198452158 with x=1, y=2, z=3, t=4 and seed=12341234', () => {
    expect(SquirrelRNG.Get4dNoiseNegOneToOne(1, 2, 3, 4, 12341234)).toBe(
      -0.5047005198452158
    );
  });

  it('Get1dNoiseUint returns 377036288 with x=0 and seed=0', () => {
    expect(SquirrelRNG.Get1dNoiseUint(0, 0)).toBe(377036288);
  });
  it('Get1dNoiseUint returns 1849206094 with x=999999999 and seed=10000', () => {
    expect(SquirrelRNG.Get1dNoiseUint(10000, 999999999)).toBe(1849206094);
  });
  it('Get1dNoiseUint returns 2330831769 with x=500000 and seed=999999999', () => {
    expect(SquirrelRNG.Get1dNoiseUint(999999999, 500000)).toBe(2330831769);
  });

  it('rollRandomIntInRange returns the same number given the same input', () => {
    const rng = new SquirrelRNG(9234587908345);
    const value1 = rng.rollRandomIntInRange(1, 1000000);
    rng.setPosition(0);
    const value2 = rng.rollRandomIntInRange(1, 1000000);
    expect(value1).toBe(value2);
  });
});
