export default class SquirrelRNG {
  #seed;
  #position;

  static #SQ5_BIT_NOISE1 = BigInt(0xd2a80a3f); // 11010010101010000000101000111111
  static #SQ5_BIT_NOISE2 = BigInt(0xa884f197); // 10101000100001001111000110010111
  static #SQ5_BIT_NOISE3 = BigInt(0x6c736f4b); // 01101100011100110110111101001011
  static #SQ5_BIT_NOISE4 = BigInt(0xb79f3abb); // 10110111100111110011101010111011
  static #SQ5_BIT_NOISE5 = BigInt(0x1b56c4f5); // 00011011010101101100010011110101
  static #PRIME1 = 198491317; // Large prime number with non-boring bits
  static #PRIME2 = 6542989; // Large prime number with distinct and non-boring bits
  static #PRIME3 = 357239; // Large prime number with distinct and non-boring bits
  static #ONE_OVER_MAX_UINT = 1.0 / 0xffffffff;
  static #ONE_OVER_MAX_INT = 1.0 / 0x7fffffff;

  constructor(seed, position) {
    this.#seed = seed || 0;
    this.#position = position || 0;
  }

  static #SquirrelNoise5(position, seed) {
    let mangledBits = BigInt(position);
    mangledBits = BigInt.asUintN(32, mangledBits * SquirrelRNG.#SQ5_BIT_NOISE1);
    mangledBits = BigInt.asUintN(32, mangledBits + BigInt(seed || 0n));
    mangledBits ^= mangledBits >> 9n;
    mangledBits = BigInt.asUintN(32, mangledBits + SquirrelRNG.#SQ5_BIT_NOISE2);
    mangledBits ^= mangledBits >> 11n;
    mangledBits = BigInt.asUintN(32, SquirrelRNG.#SQ5_BIT_NOISE3 * mangledBits);
    mangledBits ^= mangledBits >> 13n;
    mangledBits = BigInt.asUintN(32, SquirrelRNG.#SQ5_BIT_NOISE4 + mangledBits);
    mangledBits ^= mangledBits >> 15n;
    mangledBits = BigInt.asUintN(32, SquirrelRNG.#SQ5_BIT_NOISE5 * mangledBits);
    mangledBits ^= mangledBits >> 17n;
    return Number(mangledBits) + 0;
  }

  // SRand-like (seed-related) methods
  resetSeed(seed, position) {
    this.#seed = seed || 0;
    this.#position = position || 0;
  }
  getSeed() {
    return this.#seed;
  }
  setPosition(position) {
    this.#position = position || 0;
  }
  getPosition() {
    return this.#position;
  }

  // Rand-like (sequenctial random rolls) methods; each one advances the RNG to its next position
  rollRandomIntLessThan(maxValueNotInclusive) {
    return Math.floor(
      SquirrelRNG.Get1dNoiseZeroToOne(this.#position++, this.#seed) *
        maxValueNotInclusive
    );
  }
  rollRandomIntInRange(minValueInclusive, maxValueNotInclusive) {
    return Math.floor(
      SquirrelRNG.Get1dNoiseZeroToOne(this.#position++, this.#seed) *
        (maxValueNotInclusive - minValueInclusive) +
        minValueInclusive
    );
  }
  rollRandomChance(probabilityOfReturningTrue) {
    return (
      SquirrelRNG.Get1dNoiseZeroToOne(this.#position++, this.#seed) <
      probabilityOfReturningTrue
    );
  }
  rollRandomFloatZeroToOne() {
    return SquirrelRNG.Get1dNoiseZeroToOne(this.#position++, this.#seed);
  }
  rollRandomFloatInRange(minValueInclusive, maxValueInclusive) {
    return (
      SquirrelRNG.Get1dNoiseZeroToOne(this.#position++, this.#seed) *
        (maxValueInclusive - minValueInclusive) +
      minValueInclusive
    );
  }

  //-----------------------------------------------------------------------------------------------
  // Raw pseudorandom noise functions (random-access / deterministic).  Basis of all other noise.
  //
  static Get1dNoiseUint(indexX = 0, seed = 0) {
    return SquirrelRNG.#SquirrelNoise5(indexX, seed);
  }
  static Get2dNoiseUint(indexX, indexY, seed) {
    return SquirrelRNG.#SquirrelNoise5(
      indexX + SquirrelRNG.#PRIME1 * indexY,
      seed
    );
  }
  static Get3dNoiseUint(indexX, indexY, indexZ, seed) {
    return SquirrelRNG.#SquirrelNoise5(
      indexX + SquirrelRNG.#PRIME1 * indexY + SquirrelRNG.#PRIME2 * indexZ,
      seed
    );
  }
  static Get4dNoiseUint(indexX, indexY, indexZ, indexT, seed) {
    return SquirrelRNG.#SquirrelNoise5(
      indexX +
        SquirrelRNG.#PRIME1 * indexY +
        SquirrelRNG.#PRIME2 * indexZ +
        SquirrelRNG.#PRIME3 * indexT,
      seed
    );
  }

  //-----------------------------------------------------------------------------------------------
  // Same functions, mapped to floats in [0,1] for convenience.
  //
  static Get1dNoiseZeroToOne(indexX, seed) {
    return SquirrelRNG.#ONE_OVER_MAX_UINT * this.Get1dNoiseUint(indexX, seed);
  }
  static Get2dNoiseZeroToOne(indexX, indexY, seed) {
    return (
      SquirrelRNG.#ONE_OVER_MAX_UINT * this.Get2dNoiseUint(indexX, indexY, seed)
    );
  }
  static Get3dNoiseZeroToOne(indexX, indexY, indexZ, seed) {
    return (
      SquirrelRNG.#ONE_OVER_MAX_UINT *
      this.Get3dNoiseUint(indexX, indexY, indexZ, seed)
    );
  }
  static Get4dNoiseZeroToOne(indexX, indexY, indexZ, indexT, seed) {
    return (
      SquirrelRNG.#ONE_OVER_MAX_UINT *
      this.Get4dNoiseUint(indexX, indexY, indexZ, indexT, seed)
    );
  }

  //-----------------------------------------------------------------------------------------------
  // Same functions, mapped to floats in [-1,1] for convenience.
  //
  static Get1dNoiseNegOneToOne(index, seed) {
    return (
      SquirrelRNG.#ONE_OVER_MAX_INT * (this.#SquirrelNoise5(index, seed) | 0)
    );
  }

  static Get2dNoiseNegOneToOne(indexX, indexY, seed) {
    return (
      SquirrelRNG.#ONE_OVER_MAX_INT *
      (this.Get2dNoiseUint(indexX, indexY, seed) | 0)
    );
  }

  static Get3dNoiseNegOneToOne(indexX, indexY, indexZ, seed) {
    return (
      SquirrelRNG.#ONE_OVER_MAX_INT *
      (this.Get3dNoiseUint(indexX, indexY, indexZ, seed) | 0)
    );
  }

  static Get4dNoiseNegOneToOne(indexX, indexY, indexZ, indexT, seed) {
    return (
      SquirrelRNG.#ONE_OVER_MAX_INT *
      (this.Get4dNoiseUint(indexX, indexY, indexZ, indexT, seed) | 0)
    );
  }
}
