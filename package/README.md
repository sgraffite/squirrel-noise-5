# Squirrel Noise 5

`squirrel-noise-5` is a utility to generate deterministic random numbers.

This an adaptation of Squirrel Eiserloh's: Squirrel's Raw Noise utilities (version 5)

Squirrel Eiserloh's original GDC talk on YouTube:
<a href="https://www.youtube.com/watch?v=LWFzPP8ZbdU">Noise-Based RNG</a>

Newer version since the GDC talk, Squirrel Noise 5 (with improved statistical quality with some help from mathematician Peter Schmidt-Nielsen):
<a href="http://eiserloh.net/noise/SquirrelNoise5.hpp">Squirrel Noise 5</a>

## Description

This allows generation of seeded sequential random values, seeded tables of random values (1D, 2D, 3D, 4D). All of these are order independant and allow for random access.

### Changes I made compared to the C++ version:

The first change I made was inside the SquirrelNoise5 function. I used 64 bit integer space during addition and multiplication steps, and then immediately converted back to an unsigned 32 bit integer. This achieved bit perfect results when compared against the original C++ code output.

The second change is for the functions that return a decimal between x and y, the C++ floats only return 6-9 digits after the decimal. This javascript version returns quite a few more decimal places. Rather than try to round it to make it exactly the same, I left it as is as I couldn't think of a scenario where more precision would hurt.

## Installation

```
npm i squirrel-noise-5
```

## Usage

### Static example

```javascript
import SquirrelRNG from 'squirrel-noise-5';

const index = 0;
const seed = 123456;
const randomIntValue = SquirrelRNG.Get1dNoiseUint(index, seed);
const randomFloatValue = SquirrelRNG.Get1dNoiseZeroToOne(index, seed);
```

### Instance example

```javascript
import SquirrelRNG from 'squirrel-noise-5';

const position = 0;
const seed = 123456;
const rng = new SquirrelRNG(seed, position);
// Roll a D20
const randomIntValue = rng.rollRandomIntInRange(1, 21);

// Roll a D20 again
const randomIntValue2 = rng.rollRandomIntInRange(1, 21);
```

### Static 2D example

```javascript
import SquirrelRNG from 'squirrel-noise-5';

const playerSeed = 123456;
const playerCoordinates = { x: 300, y: 72 };
const randomIntValue = SquirrelRNG.Get2dNoiseUint(
  playerCoordinates.x,
  playerCoordinates.y,
  playerSeed
);
```

### Instance random access example

```javascript
import SquirrelRNG from 'squirrel-noise-5';

const seed = 123456;
const gameRound = 12;
const rng = new SquirrelRNG(seed, gameRound);
// Roll a D20
const randomIntValue = rng.rollRandomIntInRange(1, 21);

// Position incremented to 13 internally, set it back to 12.
rng.setPosition(gameRound);

// Roll a D20, result will be the same as the first roll.
const randomIntValue2 = rng.rollRandomIntInRange(1, 21);
```

### How to run the unit tests

```
npm install -D vitest
npm run test
```

## Authors and acknowledgment

I want to thank the original author Squirrel Eiserloh for making this available to the public.

## License

<a href="https://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0</a>
